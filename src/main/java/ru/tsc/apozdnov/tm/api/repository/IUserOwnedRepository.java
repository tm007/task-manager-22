package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    List<M> findAll(String userId);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M add(String userId, M model);

    List<M> findAll(String userId, Comparator comparator);

    List<M> findAll(String userId, Sort sort);

}
