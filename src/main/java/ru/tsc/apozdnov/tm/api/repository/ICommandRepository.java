package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand abstractCommand);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String arg);

}
