package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-change-by-index";
    }

    @Override
    public String getDescription() {
        return "Change project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** CHANGE PROJECT STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

}
