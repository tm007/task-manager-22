package ru.tsc.apozdnov.tm.command.user;

import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public String getDescription() {
        return "User unlock";
    }

    @Override
    public RoleType[] getRoleType() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK:]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

}
