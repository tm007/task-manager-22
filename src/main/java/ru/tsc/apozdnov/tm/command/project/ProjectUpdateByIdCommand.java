package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Update project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** UPDATE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getProjectService().updateById(userId, id, name, description);
    }

}
