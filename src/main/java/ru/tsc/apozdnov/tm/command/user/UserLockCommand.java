package ru.tsc.apozdnov.tm.command.user;

import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "User lock!!!";
    }

    @Override
    public RoleType[] getRoleType() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK:]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

}
