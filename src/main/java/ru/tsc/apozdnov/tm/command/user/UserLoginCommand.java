package ru.tsc.apozdnov.tm.command.user;

import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "Log in";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public RoleType[] getRoleType() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}