package ru.tsc.apozdnov.tm.command.user;

import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class UserUpdateCommand extends AbstractUserCommand {

    public static final String NAME = "user-update";

    public static final String DESCRIPTION = "Update user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public RoleType[] getRoleType() {
        return null;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER FIRST NAME:]");
        String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().userUpdate(userId, firstName, lastName, middleName);
    }

}