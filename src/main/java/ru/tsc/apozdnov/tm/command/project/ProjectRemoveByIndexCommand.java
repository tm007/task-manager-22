package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Date;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** REMOVE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
