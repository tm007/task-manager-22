package ru.tsc.apozdnov.tm.command.task;

import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Date;

public class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";

    public static final String DESCRIPTION = "Create new task.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("[ENTER DATE END:]");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        serviceLocator.getTaskService().create(userId, name, description, dateBegin, dateEnd);
    }

}
