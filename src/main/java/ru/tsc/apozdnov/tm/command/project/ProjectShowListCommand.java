package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("**** PROJECTS LIST ****");
        System.out.println("**** ENTER SORT ****");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId, sort);
        for (Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
            serviceLocator.getloggerService().info(project.toString());
        }
    }

}
