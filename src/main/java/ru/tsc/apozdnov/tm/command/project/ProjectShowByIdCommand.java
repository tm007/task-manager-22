package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** SHOW PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
