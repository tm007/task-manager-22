package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(RoleType.USUAL);
        return add(user);
    }

    @Override
    public User findOneByLogin(final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final RoleType role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findOneByEmail(final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(final String login) {
        return models.stream().anyMatch(user -> login.equals(user.getLogin()));
    }

    @Override
    public boolean isEmailExist(final String email) {
        return models.stream().anyMatch(user -> email.equals(user.getEmail()));
    }

}
