package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.service.ILoggerService;
import ru.tsc.apozdnov.tm.exception.system.FileOrDirectoryNotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    private static final String LOG_DIRECTORY = "log";

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = Paths.get(LOG_DIRECTORY, "commands.xml").toString();

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = Paths.get(LOG_DIRECTORY, "errors.xml").toString();

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = Paths.get(LOG_DIRECTORY, "messages.xml").toString();

    private final LogManager logManager = LogManager.getLogManager();

    private final Logger rootLogger = Logger.getLogger("");

    private final Logger commandsLogger = Logger.getLogger(COMMANDS);

    private final Logger errorsLogger = Logger.getLogger(ERRORS);

    private final Logger messagesLogger = Logger.getLogger(MESSAGES);

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        checkLogDirectory();
        init();
        registry(commandsLogger, COMMANDS_FILE, false);
        registry(errorsLogger, ERRORS_FILE, true);
        registry(messagesLogger, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            logManager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private void checkLogDirectory() {
        final Path logsDirectoryPath = Paths.get(LOG_DIRECTORY);
        if (Files.exists(logsDirectoryPath)) {
            if (!Files.isDirectory(logsDirectoryPath) || !Files.isWritable(logsDirectoryPath)) {
                throw new FileOrDirectoryNotFoundException();
            }
        } else {
            try {
                Files.createDirectory(logsDirectoryPath);
            } catch (IOException e) {
                rootLogger.severe(e.getMessage());
            }
        }
    }

    private void registry(final Logger logger, final String logFileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(logFileName));
        } catch (final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler consoleHandler = new ConsoleHandler();
        final Formatter formatter = new Formatter() {
            @Override
            public String format(final LogRecord record) {
                return record.getMessage() + "\n";
            }
        };
        consoleHandler.setFormatter(formatter);
        return consoleHandler;
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messagesLogger.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messagesLogger.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commandsLogger.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errorsLogger.log(Level.SEVERE, e.getMessage(), e);
    }

}
