package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String userdId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        final Project project = Optional.ofNullable(projectRepository.findOneById(projectId)).orElseThrow(ProjectNotFoundException::new);
        projectRepository.remove(project);
        taskRepository.findAllByProjectId(userdId, projectId)
                .stream()
                .forEach(task -> taskRepository.removeById(userdId, task.getId()));
        projectRepository.removeById(userdId, projectId);
    }

}
