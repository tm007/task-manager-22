package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IRepository;
import ru.tsc.apozdnov.tm.api.service.IService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll();
    }

    @Override
    public void removeAll(final Collection<M> collection) {
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
