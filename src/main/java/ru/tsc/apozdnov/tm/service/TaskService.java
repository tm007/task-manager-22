package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.field.NameEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final Task task = Optional.ofNullable(create(name, description)).orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task remove(String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        return repository.remove(task);
    }

    @Override
    public List<Task> findAll(String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll();
    }

    @Override
    public List<Task> findAllByProjectId(String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = Optional.ofNullable(findOneById(id)).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateByIndex(String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = Optional.ofNullable(findOneByIndex(index)).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = Optional.ofNullable(findOneById(id)).orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = Optional.ofNullable(findOneByIndex(index)).orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}
