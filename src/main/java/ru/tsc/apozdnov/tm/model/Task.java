package ru.tsc.apozdnov.tm.model;

import ru.tsc.apozdnov.tm.api.model.IWBS;
import ru.tsc.apozdnov.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private String projectId;

    private Date dateCreated = new Date();

    private Date dateBegin = new Date();

    private Date dateEnd = new Date();

    public Task() {
    }

    public Task(final String name, final Status status, final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(final Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(final Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "NAME" + ": " + name + "  " +
                "DESCRIPTION: " + ": " + description + "  " +
                "PROJECTID:" + projectId + "\n";
    }

}
