package ru.tsc.apozdnov.tm.enumerated;

import ru.tsc.apozdnov.tm.comarator.DateBeginComparator;
import ru.tsc.apozdnov.tm.comarator.DateCreatedComparator;
import ru.tsc.apozdnov.tm.comarator.NameComarator;
import ru.tsc.apozdnov.tm.comarator.StatusComarator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("SORT BY NAME", NameComarator.INSTANCE),
    BY_STATUS("SORT BY STATUS", StatusComarator.INSTANCE),
    BY_CREATED("SORT BY CREATED", DateCreatedComparator.INSTANCE),
    BY_DATE_BEGIN("SORT BY DATE BEGIN", DateBeginComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(String val) {
        if (val == null || val.isEmpty()) return null;
        for (Sort sort : values()) {
            if (sort.name().equals(val)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }
}
