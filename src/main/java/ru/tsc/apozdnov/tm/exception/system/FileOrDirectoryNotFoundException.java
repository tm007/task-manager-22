package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class FileOrDirectoryNotFoundException extends AbstractException {

    public FileOrDirectoryNotFoundException() {
        super("FAULT!! File or Directory does not exist!!!!");
    }

}
