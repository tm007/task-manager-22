package ru.tsc.apozdnov.tm.exception.user;

public class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error!!! Password is empty or incrorrect!");
    }

}
